class QueryBuilder
{
	protected $connection;
	protected $query;
	protected $parameters;
	protected $alias;

	public function __construct(BDDInterface $connection = NULL)
	{

	}

	public function select(string $values = '*'): QueryBuilder
	{
		$builder->select('title, content, date');
		$query = $builder->get();
	}

	public function from(string $table, string $alias): QueryBuilder
	{
		$builder->select('title, content, date');
		$builder->from('hqln');
		$query = $builder->get();
	}

	public function where(string $condition): QueryBuilder
	{
		$builder->where('name', $name);
		$builder->where('title', $title);
		$builder->where('status', $status);
	}

	public function setParameters(string $key, string $value): QueryBuilder
	{

	}

	public function join(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
	{
		$builder->db->table('blog');
		$builder->select('*');
		$builder->join('comments', 'comments.id = blogs.id');
		$query = $builder->get();
	}

	public function leftjoin(string $table, string $aliasTarget, string $fieldSource ='id', string $fieldTarget ='id'): QueryBuilder
	{
		$builder->join('comments', 'comments.id = blogs.id', 'left');
	}

	public function addToQuery(string $query): QueryBuilder
	{

	}

	public function getQuery(): ResultInterface
	{
		$builder = $db->table('hqln');
		$query   = $builder->get();
	}

}
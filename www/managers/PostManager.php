public function getUserPost(int $id)
{
	return (new QueryBuilder())
		->select('p.*, u.*')
		->from('hqln_post', 'p')
		->join('hqln_users', 'u')
		->where('p.author = :iduser')
		->setParameter('iduser', $id)
		->getQuery()
		->geArrayResult(Post::class)
}
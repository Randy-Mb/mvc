<?php

namespace www\managers;

use www\core\DB;
use www\models\users;

class userManager extends DB {

	public function __construct()
	{
		parent::__construct(users::class, 'users');
	}
}
<?php

namespace www\controllers;

use www\managers\UserManager;

use www\models\users;
use www\core\view;

class UserController
{
    public function defaultAction()
    {
        echo "Action default dans le controller user";
    }

    public function addAction()
    {
        echo "Action add dans le controller user";
    }
    

    public function loginAction()
    {
        $myView = new View("login", "account");
    }



    public function registerAction()
    {

        $configForm  = users::getRegisterForm();

        if( $_SERVER["REQUEST_METHOD"] == "POST"){

            //Vérification des champs
            $errors = Validator::formValidate( $configForm, $_POST );
            print_r($errors);

        }


        $user = new users();
        $user->setId(1);
        $user->setFirstname("Yves");
        $user->save();


        $myView = new View("register", "account");
        $myView->assign("configForm", $configForm);
    }



    public function forgotPwdAction()
    {
        $myView = new View("forgotPwd", "account");
    }


    public funtion hydrate(array $donnees)
    {
        foreach ($donnees as $key => $value) {
            // On recupere le nom du setter correspondant à l'attribut
            $method = 'set'.ucfirst($key);

            // Si le setter correspondant existe.
            if (method_exists($this, $method))
            {
                // On appelle le setter
                $this->$method($value);
            }
        }
    }
}